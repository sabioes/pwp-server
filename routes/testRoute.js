var express = require('express');
/*--------------------------------------
    ROUTES FOR API
--------------------------------------*/
//get an instance of the express Router
var router = express.Router();

//test route ('/') to make sure everything is working(http://localhost:8080/api)
router.get('/', function(req, res){
    res.json({message: 'Le voilà! Bonjour le monde'});
});

module.exports = router;