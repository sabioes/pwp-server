//server.js

/*--------------------------------------
    BASE SETUP
--------------------------------------*/
//call the packages that we need
var express = require('express');
var app     = express();
var port    =   process.env.PORT || 8090;
var testRouter = require('./routes/testRoute');
var sendmailRouter = require('./routes/sendmailRoute');

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
app.use('/test', testRouter);
app.use('/sendmail', sendmailRouter);
app.get('/', function (req, res) {
  res.send('Hello World')
})

app.listen(port);
console.log('Magic happens on port ' + port);

