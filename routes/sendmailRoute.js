var express = require('express');
var bodyParser = require("body-parser");

const serviceMail = require('../service/mail');

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

/*--------------------------------------
    ROUTES FOR API
--------------------------------------*/
//get an instance of the express Router
var router = express();
//create application/json parser
var jsonParser = bodyParser.json()
// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })
// POST /sendmail gets JSON bodies
router.post('/', jsonParser, function (req, res) {
    console.log("json post detected");
    let response;

    if (validateEmail(req.body.data[1].value)) {
        let result = serviceMail.sendMail(
            req.body.data[0].value,
            req.body.data[1].value,
            req.body.data[2].value,
            req.body.data[3].value
        );

        response = JSON.parse(JSON.stringify({
            "response":
                [
                    { result: "OK" },
                    { description: "email sent" }
                ]
        }));

        res.status(200);
    } else {
        response = JSON.parse(JSON.stringify({
            "response":
                [
                    { result: "ERROR" },
                    { description: "email not valid" }
                ]
        }));
        res.status(400);
    }

    res.send(response);
});
router.post('/', urlencodedParser, function (req, res) {
    console.log(req.body);
    console.log("post detected");
});
router.post('/', function (req, res) {
    console.log(req.body);
    console.log("post detected");
});

module.exports = router;